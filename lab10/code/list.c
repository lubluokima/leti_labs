#define MAXLEN 256
#include <stdlib.h>
#include "list.h"

void createList(list_t **head, FILE *file)
{
  list_t *temp = NULL;
  char line[MAXLEN];
  if (fgets(line, MAXLEN, file) != NULL)
  {
    *head = malloc(sizeof(list_t));
    if(*head == NULL)
    {
      puts("Memory error while list initialization");
    }
    else
    {
      /* Init head firstly to simplify the while-cycle */
      (*head)->data = createNode(line);
      (*head)->next = NULL;

      temp = *head;

      while (fgets(line, MAXLEN, file) != NULL)
      {
        (temp->next) = malloc(sizeof(list_t));
        temp = (temp->next);
        temp->data = createNode(line);
      }
      (temp->next) = NULL;
    }
  }
  else
  {
    puts("The file must contain data.");
  }
}

void printList(list_t *head, FILE *file)
{
  list_t *temp;
  temp = head;

  fprintf(file, "________________________________________________________________________________________________\n");
  fprintf(file, "|%20s |%10s |%19s |%5s |%9s |%9s |%9s |\n", "NAME", "COUNTRY", "MARKS", "YEAR", "QUANTITY", "RATE", "INTEREST");
  fprintf(file, "|---------------------+-----------+--------------------+------+----------+----------+----------|\n");
  fprintf(file, "|---------------------+-----------+------+------+------+------+----------+----------+----------|\n");

  while (temp != NULL)
  {
    printItem(temp->data, file); /* One structure item to console */
    temp = temp->next;
  }

  fprintf(file, "================================================================================================\n");
}

void disposeItem(list_t **node)
{
  freeStructFields((*node)->data);

  free((*node)->data);
  (*node)->data = NULL;
  (*node)->next = NULL;

  free(*node);
  (*node) = NULL;
}

void deleteNode(list_t **head)
{
  list_t *temp = *head;
  list_t *ptr = NULL;
  int count = 0;
  /* To count quantity of list elements */
  while (temp != NULL)
  {
    temp = temp->next;
    count = count + 1;
  }
  /* Task processing */
  temp = *head;
  if (count == 1)
  {
    puts("The list must have 2 or more items");
  }
  else if (count == 2)
  {
    /* Remove head of the list */
    (*head) = (*head)->next;
    disposeItem(&temp);
  }
  else
  {
    /* Delete the penultimate element */
    while (count != 3)
    {
      temp = temp->next;
      count = count - 1;
    }
    ptr = temp->next;
    temp->next = ptr->next;
    disposeItem(&ptr);
  }
}

void freeList(list_t **head)
{
  list_t *temp = NULL;
  while (*head != NULL)
  {
    temp = *head;
    *head = (*head)->next;
    disposeItem(&temp);
  }
}
