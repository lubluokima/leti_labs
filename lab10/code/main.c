#include "list.h"
#include "iofileswork.h"

#define INP "input.csv"
#define OUT "output.txt"

int main()
{
  list_t *head = NULL;
  FILE *input_f = NULL;
  FILE *output_f = NULL;

  /* File-opening with check */
  opening(&input_f, INP, "r");
  if (input_f != NULL)
  {
    /* If file was open correctly
    // Creating list with data from file */
    createList(&head, input_f);
    closing(&input_f);

    if (head == NULL)
    {
      puts("\nList was now created. Program error.");
    }
    else
    {
      /* If list was created we start processing */
      opening(&output_f, OUT, "w");
      if (output_f != NULL)
      {
        puts("\nInput done. The resulting list printed in output file");
        /* Printing the list to file */
        fprintf(output_f, "Information that program got from input file:\n");
        printList(head, output_f);

        /* List processing by task */
        deleteNode(&head);

        puts("\nProcessing result printed in output file");
        /* Printing the list after processing */
        fprintf(output_f, "Information after processing:\n");
        printList(head, output_f);

        closing(&output_f);
      }
      else
      {
        puts("Program is not complete");
        puts("List was created but some errors crashed output file");
        puts("Disposing list from heap");
      }
      /* Dispose list and finishing program */
      freeList(&head);
    }
  }
  return 0;
}
