#ifndef IOFILESWORK_H_
#define IOFILESWORK_H_

/*Function for file opening with error-checks
//Input param: input-file pointer
//             file name in the system
//             opening option
////////////////////////////////////////////////////////////////////*/
void opening(FILE **file, char *name, char *option);

/*Function for file closing
//Input param: pointer for file to close
////////////////////////////////////////////////////////////////////*/
void closing(FILE **file);

#endif /* IOFILESWORK_H_ */
