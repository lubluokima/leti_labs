#ifndef LIST_H_
#define LIST_H_

#include "gunstructure.h"

/*Definition of a structure type for a list
////////////////////////////////////////////////////////////////////*/
struct LNode
{
  gun_t *data;        /* Information field */
  struct LNode *next; /* Pointer to next element */
};

/*Definition of the type name for uses
////////////////////////////////////////////////////////////////////*/
typedef struct LNode list_t;

/*Creating list with input file
//Input param: head-pointer of list
//             file-pointer for input file
////////////////////////////////////////////////////////////////////*/
void createList(list_t **head, FILE *file);

/*Print every element of list to the console
//Input param: head-pointer of list
////////////////////////////////////////////////////////////////////*/
void printList(list_t *head, FILE *file);

/*Dispose one node from the list, supporting function
//Input param: pointer to the node element
////////////////////////////////////////////////////////////////////*/
void disposeItem(list_t **node);

/*Delete the penultimate element
//Input param: head-pointer of list
////////////////////////////////////////////////////////////////////*/
void deleteNode(list_t **head);

/*Dispose list from memory
//Input param: head-pointer of list
////////////////////////////////////////////////////////////////////*/
void freeList(list_t **head);

#endif /* LIST_H_ */
