#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "itemarr.h"
#include "iofileswork.h"
#define MAXLEN 256
#define WORK "work.csv"

void freeArray(gun_t ***items, int *amount)
{
  int i;

  for (i = 0; i < (*amount); i = i + 1)
  {
    freeStructFields((*items)[i]);
  }
  free(*items);
  *items = NULL;
  *amount = 0;
}

gun_t **createArray(int *amount, FILE *file)
{
  char line[MAXLEN];
  int quantity = 0;
  int check_count = 0;
  int i = 0;
  gun_t **guns = NULL;

  while (fgets(line, MAXLEN, file) != NULL)
  {
    quantity = quantity + 1;
  }
  rewind(file);
  guns = (gun_t**)malloc(sizeof(gun_t*) * quantity);
  if (guns != NULL)
  {
    for (i = 0; i < quantity; i = i + 1)
    {
      fgets(line, MAXLEN, file);
      guns[i] = createNode(line);
      if (guns[i] != NULL)
      {
        check_count = check_count + 1;
      }
      else
      {
        i = quantity;
      }
    }
    if (check_count != quantity)
    {
      freeArray(&guns, &check_count);
      quantity = 0;
    }
  }
  *amount = quantity;
  return guns;
}

void printArray(gun_t **items, int amount, FILE *file)
{
  int i;
  fprintf(file, "________________________________________________________________________________________________\n");
  fprintf(file, "|%20s |%10s |%19s |%5s |%9s |%9s |%9s |\n", "NAME", "COUNTRY", "MARKS", "YEAR", "QUANTITY", "RATE", "INTEREST");
  fprintf(file, "|---------------------+-----------+--------------------+------+----------+----------+----------|\n");
  fprintf(file, "|---------------------+-----------+------+------+------+------+----------+----------+----------|\n");
  for (i = 0; i < amount; i = i + 1)
  {
    printItem(items[i], stdout);
  }
  fprintf(file, "================================================================================================\n");
}

void addItem(gun_t ***items, int *amount, char *line, int *success)
{
  gun_t **temp = NULL;
  temp = (gun_t**)realloc(*items, (1 + (*amount)) * sizeof(gun_t*));
  if (temp == NULL)
  {
    *success = 0;
  }
  else
  {
    *items = temp;
    (*items)[(*amount)] = createNode(line);
    if ((*items)[(*amount)] == NULL)
    {
      freeArray(items, amount);
      *success = 0;
    }
    else
    {
      *amount = (*amount) + 1;
    }
  }
}

char *name_e(gun_t **items, int number)
{
  return (items[number])->name;
}

char *country_e(gun_t **items, int number)
{
  return (items[number])->country;
}

int strPartCaseCmp(char *sz1, char *sz2, int qty)
{
  int i;
  int result = 0;

  for (i = 0; i < qty; i = i + 1)
  {
    if (tolower(sz1[i]) != tolower(sz2[i]))
    {
      result = 1;
      i = qty;
    }
  }
  return result;
}

gun_t **chooseItems(int *qty_new, char *keyword, gun_t **items, int amount, char* (*funcName)(gun_t**, int))
{
  int i = 0;
  int check = 0;
  FILE *temp = NULL;
  gun_t **chosen = NULL;

  opening(&temp, WORK, "w+");
  if (temp != NULL)
  {
    for (i = 0; i < amount; i = i + 1)
    {
      if (!strPartCaseCmp(keyword, funcName(items, i), strlen(keyword)))
      {
        check = check + 1;
        fprintf(temp, "%s;", items[i]->name);
        fprintf(temp, "%s;", items[i]->country);
        fprintf(temp, "%d;", items[i]->mark[0]);
        fprintf(temp, "%d;", items[i]->mark[1]);
        fprintf(temp, "%d;", items[i]->mark[2]);
        fprintf(temp, "%d;", items[i]->year);
        fprintf(temp, "%d;", items[i]->quantity);
        fprintf(temp, "%f;", items[i]->rate);
        fprintf(temp, "%f\n", items[i]->interest);
      }
    }
    if (check != 0)
    {
      rewind(temp);
      chosen = createArray(&check, temp);
    }
    *qty_new = check;
    closing(&temp);
    remove(WORK);
  }
  return chosen;
}

void sortArray(gun_t ***guns, int quantity)
{
  int i, j;
  gun_t *temp = NULL;

  for (i = 0; i < quantity - 1; i = i + 1)
  {
    for (j = 0; j < quantity - 1- i; j = j + 1)
    {
      if ((*guns)[j]->quantity < (*guns)[j + 1]->quantity)
      {
        temp = (*guns)[j];
        (*guns)[j] = (*guns)[j + 1];
        (*guns)[j + 1] = temp;
      }
    }
  }
}
