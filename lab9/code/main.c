#include "menu.h"
#include "iofileswork.h"
#define INP "input.csv"

int main()
{
  int amount = 0;
  gun_t **struct_arr = NULL;
  FILE *input_f = NULL;

  /* File-opening with check */
  opening(&input_f, INP, "r");
  if (input_f != NULL)
  {
    /* If file was open correctly
    // Creating array with data from file */
    struct_arr = createArray(&amount, input_f);
    closing(&input_f);

    if (struct_arr != NULL)
    {
      /* If array was created we start processing */
      menu(&struct_arr, &amount);
    }
  }
  return 0;
}
