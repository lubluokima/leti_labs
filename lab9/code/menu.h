#ifndef MENU_H_
#define MENU_H_

#include "itemarr.h"
/*Interface function for more comfort processing
//User can:
//          1 - print array data into console
//          2 - add one structure element
//          3 - choose field and sort elements
//          0 - stop the program
//Input param: pointer to array of pointers for structure
//             pointer to variable storing array size
//////////////////////////////////////////////////////////*/
void menu(gun_t ***items, int *amount);


/*Clear console screen and print table with structure array
//Input param: array of pointers ti structure
//             variable storing array size
//             file where program will write
//////////////////////////////////////////////////////////*/
void printTable(gun_t **items, int amount, FILE *file);

/* Menu appearence */
void printMainMenu();

#endif /* MENU_H_ */
