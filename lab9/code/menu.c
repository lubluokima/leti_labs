#include <stdlib.h>
#include <string.h>
#include "itemarr.h"
#define CLRSCR_ system("cls")
#define MAXLEN 256

void printMainMenu()
{
  CLRSCR_;
  puts(">>_______________MAIN MENU_______________<<");
  puts(">> (1) print the bracket with array data <<");
  puts(">> (2) add structure element via console <<");
  puts(">> (3) select and sort elements by task  <<");
  puts(">> (0) finish program and print results  <<");
  puts(">>=======================================<<");
  puts(">> PRINT COMMAND-NUMBER HERE: ");
  printf(">> ");
}

void printTable(gun_t **items, int amount, FILE *file)
{
  CLRSCR_;
  puts(">> Table with data from array: ");
  printArray(items, amount, file);
}

void menu(gun_t ***items, int *amount)
{
  int key = 0;
  int number;
  char line[MAXLEN];

  /* program select items, sort them and put to this array */
  gun_t **sorted = NULL;

  /* array of pointers to functions for structure selection */
  char *(*func[2])(gun_t**, int);
  func[0] = name_e;
  func[1] = country_e;

  do
  {
    printMainMenu();
    scanf("%d/n", &key);

    switch (key)
    {
      case 1:
        printTable(*items, *amount, stdout);
        puts(">> Press any button to continue...");
        getchar(); getchar();
        break;
    /*________________*/
      case 2:
        CLRSCR_;
        puts(">> Print string with delimiters as in input file:");
        printf(">> ");
        scanf("%s", line);
        number = strlen(line);
        line[number] = '\n';
        line[number + 1] = '\0';
        addItem(items, amount, line, &key);
        if (key == 0)
        {
          puts(">> Not enough memory, finishing program...");
        }
        break;
    /*________________*/
      case 3:
        CLRSCR_;
        do
        {
          printTable(*items, *amount, stdout);
          puts(">>____Field for selection____<< ");
          puts(">> (1) weapon names          <<");
          puts(">> (2) producing countries   <<");
          puts(">>===========================<<");
          puts(">> PRINT COMMAND-NUMBER HERE: ");
          printf(">> ");
          scanf("%d/n", &key);

          switch(key)
          {
            case 1:
              break;
          /*________________*/
            case 2:
              break;
          /*________________*/
            default:
              CLRSCR_;
              puts(">> Incorrect command. Try again...");
              key = 0;
              getchar(); getchar();
              break;
          /*________________*/
          }
          if (key == 1 || key == 2)
          {
            printTable(*items, *amount, stdout);
            puts(">> Print substring for searching: ");
            printf(">> ");
            scanf("%s", line);
            number = 0;
            sorted = chooseItems(&number, line, *items, *amount, func[key - 1]);
            if (sorted != NULL)
            {
              sortArray(&sorted, number);
              printTable(sorted, number, stdout);
              freeArray(&sorted, &number);
              puts(">> Press any button to continue...");
              getchar(); getchar();
            }
            else if (number != 0)
            {
              CLRSCR_;
              puts(">> Some technical problems. Try again...");
              getchar(); getchar();
            }
            else
            {
              CLRSCR_;
              puts(">> There is no field containing your substring...");
              getchar(); getchar();
            }
          }
        }
        while (key != 1 && key != 2);
        break;
    /*________________*/
      case 0:
        CLRSCR_;
        puts(">> Program finished its work!");
        freeArray(items, amount);
        break;
    /*________________*/
      default:
        CLRSCR_;
        puts(">> Incorrect command. Try again...");
        getchar(); getchar();
        break;
    /*________________*/
    }
  }
  while (key != 0);
}
