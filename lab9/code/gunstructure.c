#include <string.h>
#include <stdlib.h>
#include "gunstructure.h"

void printItem(gun_t *item, FILE *file)
{
  fprintf(file, "|%21s|%11s|", item->name, item->country);
  fprintf(file, "%6d|%6d|%6d|", item->mark[0], item->mark[1], item->mark[2]);
  fprintf(file, "%5d |%9d |%9.2f |%9.2f |\n", item->year, item->quantity, item->rate, item->interest);
}

gun_t *createNode(char *line)
{
  gun_t *gun = NULL;
  char **fields = NULL;
  int field_count = 0;

  /* split our string to tokens */
  fields = stringSplit(line, &field_count);

  if (fields == NULL)
  {
    puts("Memory error with fields array.");
  }
  else
  {
    /* create one item for the list */
    gun = fillStruct(fields);

    if (gun == NULL)
    {
      puts("Memory error with item node.");
    }
    /* need to free memory after processing */
    clearStringArray(fields, field_count);
  }
  return gun;
}

char **stringSplit(char *line, int *field_quantity)
{
  char delimiters[] = ";";
  int i, j;
  int field_count = 0;

  for (i = 0; i < strlen(line); i = i + 1)
  {
    if ((strchr(delimiters, line[i]) != NULL) || (line[i] == '\n'))
    {
      field_count = field_count + 1;
    }
  }

  *field_quantity = field_count;

  int word_begin = 0;
  int word_end = 0;
  int word_length = 0;
  int field_check = 0;

  char **fields = (char**)malloc(sizeof(char*) * (field_count));
  if (fields == NULL)
  {
    puts("Memory error with array for stringSplit");
  }
  else
  {
    for (i = 0; i < strlen(line); i = i + 1)
    {
      if ((strchr(delimiters, line[i]) != NULL) || (line[i] == '\n'))
      {
        word_end = i - 1;
        word_length = word_end - word_begin + 1;

        fields[field_check] = (char*)malloc(sizeof(char) * (word_length) + 1);
        if (fields[field_check] == NULL)
        {
          puts("Memory error");
          i = strlen(line);
        }
        else
        {
            for (j = 0; j < word_length; j = j + 1)
            {
              fields[field_check][j] = line[word_begin + j];
            }
            fields[field_check][word_length] = '\0';
            word_begin = i + 1;
            field_check = field_check + 1;
          }
        }
    }
    if (field_count != field_check)
    {
      clearStringArray(fields, field_check);
    }
  }
  return fields;
}

void clearStringArray(char **arr, int quantity)
{
  int i;

  for (i = 0; i < quantity; i++)
  {
    free(arr[i]);
    arr[i] = NULL;
  }
  free(arr);
  arr = NULL;
}

gun_t *fillStruct(char **arr)
{
  gun_t *temp = NULL;
  temp = (gun_t*)malloc(sizeof(gun_t));

  if (temp != NULL)
  {
    temp->name = (char*)malloc(1 + sizeof(char) * strlen(arr[0]));
    temp->country = (char*)malloc(1 + sizeof(char) * strlen(arr[1]));
    temp->mark = (int*)malloc(sizeof(int) * 3);

    if (temp->name == NULL || temp->country == NULL || temp->mark == NULL)
    {
      puts("Memory error fillStruct");
      if (temp->name != NULL)
      {
        free(temp->name);
      }
      if (temp->country != NULL)
      {
        free(temp->country);
      }
      if (temp->mark != NULL)
      {
        free(temp->mark);
      }
      free(temp);
    }
    else
    {
      strncpy(temp->name, arr[0], strlen(arr[0]));
      temp->name[strlen(arr[0])] = '\0';

      strncpy(temp->country, arr[1], strlen(arr[1]));
      temp->country[strlen(arr[1])] = '\0';

      temp->mark[0] = atoi(arr[2]);
      temp->mark[1] = atoi(arr[3]);
      temp->mark[2] = atoi(arr[4]);

      temp->year = atoi(arr[5]);
      temp->quantity = atoi(arr[6]);

      temp->rate = atof(arr[7]);
      temp->interest = atof(arr[8]);
    }
  }
  return temp;
}

void freeStructFields(gun_t *elem)
{
  free(elem->name);
  free(elem->country);
  free(elem->mark);

  elem->name = NULL;
  elem->country = NULL;
  elem->mark = NULL;
}
