#include <stdio.h>
#include <errno.h>

void opening(FILE **file, char *name, char *option)
{
  if (name == NULL)
  {
    fprintf(stderr, "Wrong filename pointer!");
    *file = NULL;
  }
  else if (option == NULL)
  {
    fprintf(stderr, "Wrong opening option for %s!", name);
    *file = NULL;
  }
  else
  {
    *file = fopen(name, option);
    if (*file == NULL)
    {
      perror("File opening error");
    }
    else
    {
      fprintf(stdout, "file %s was opened with option %s\n", name, option);
    }
  }
}

void closing(FILE **file)
{
  if (fclose(*file) != EOF)
  {
    fprintf(stdout, "File was closed correctly!\n");
  }
  else
  {
    printf("Closing error code: %d\n", errno); /* errno.h */
  }
}
