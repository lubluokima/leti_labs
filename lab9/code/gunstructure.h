#ifndef GUNSTRUCTURE_H_
#define GUNSTRUCTURE_H_
#include <stdio.h>

/*Definition of a structure type
////////////////////////////////////////////////////////////////////*/
struct weapon
{
  char *name;
  char *country;
  int *mark;
  int year;
  int quantity;
  float rate;
  float interest;
};

/*Definition of the type name for uses
////////////////////////////////////////////////////////////////////*/
typedef struct weapon gun_t;

/*Free memory containing one structure element
//Input param: pointer to structure
////////////////////////////////////////////////////////////////////*/
void freeStructFields(gun_t *elem);

/*Create one element of structure type from input string
//Input param: string with text information with delimiters
//             length of input string
//Output: pointer to one complete data field for list element
////////////////////////////////////////////////////////////////////*/
gun_t *createNode(char *line);

/*Print data from one node of the list, supporting function
//Input param: needed pointer on structure item
//             file-pointer for output file
////////////////////////////////////////////////////////////////////*/
void printItem(gun_t *item, FILE *file);

/*Fill all the fields of gun_t structure element from words array
//Input param: token array
//Output: pointer to structure element gun_t type
////////////////////////////////////////////////////////////////////*/
gun_t *fillStruct(char **arr);

/*Function for correct memory dispose from array with words
//Input param: pointer for char array
//             amount of words
////////////////////////////////////////////////////////////////////*/
void clearStringArray(char **arr, int quantity);

/*Allow program to split string with delimiters into tokens
//Input param: pointer to string
//             int number of tokens count
//Output: array of tokens
////////////////////////////////////////////////////////////////////*/
char **stringSplit(char *line, int *field_quantity);

#endif /* GUNSTRUCTURE_H_ */
