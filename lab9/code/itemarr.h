#ifndef ITEMARR_H_
#define ITEMARR_H_

#include "gunstructure.h"

/*Free memory allocated for array of structures
//Input param: pointer to array of gun_t structures
//             pointer to variable for amount of array items
////////////////////////////////////////////////////////////////////*/
void freeArray(gun_t ***items, int *amount);

/*Create array of structure type from input file
//Input param: pointer to variable for amount of array items
//             input file pointer
//Output param: array of gun_t structures
////////////////////////////////////////////////////////////////////*/
gun_t **createArray(int *amount, FILE *file);

/*Print array to some file
//Input param: array of gun_t structures
//             variable for amount of array items
//             output file pointer
////////////////////////////////////////////////////////////////////*/
void printArray(gun_t **items, int amount, FILE *file);

/*Add one element of structure type from input string
//Input param: pointer to array of gun_t structures
//             pointer to variable for amount of array items
//             input string
//             flag showing the success of the operation
////////////////////////////////////////////////////////////////////*/
void addItem(gun_t ***items, int *amount, char *line, int *success);

/*Sorting array of pointers by last integer field rule
//Input param: pointer to array of gun_t structures
//             amount of array items
////////////////////////////////////////////////////////////////////*/
void sortArray(gun_t ***guns, int quantity);

/*Get string from name field of structure item
//Input param: array of gun_t structures
//             index of item
//Output: name string
////////////////////////////////////////////////////////////////////*/
char *name_e(gun_t **items, int number);

/*Get string from country field of structure item
//Input param: array of gun_t structures
//             index of item
//Output: country string
////////////////////////////////////////////////////////////////////*/
char *country_e(gun_t **items, int number);

/*Create a new array from chosen(by some rule) elements from array of gun_t structures
//Input param: the number of elements in the new array
//             string containing keyword for selection
//             array of gun_t structures
//             amount of array items
//             function pointer to handle multiple rules
//Output: pointer to the new array
////////////////////////////////////////////////////////////////////*/
gun_t **chooseItems(int *qty_new, char *keyword, gun_t **items, int amount, char* (*funcName)(gun_t**, int));

/*Compares a specific number of characters in a case-insensitive strings
//Input param: first string
//             second string
//             amount of chars to compare
//Output: flag-result
//        0 if equal
//        1 if not
////////////////////////////////////////////////////////////////////*/
int strPartCaseCmp(char *sz1, char *sz2, int qty);

#endif /* ITEMARR_H_ */
